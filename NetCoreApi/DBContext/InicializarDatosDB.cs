﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using NetCoreApi.Controllers;
using NetCoreApi.Datos;
using System;
using System.Linq;

namespace NetCoreApi.DBContext
{
    public class InicializarDatosDB
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetRequiredService<EscupiendoCodigoDbContext>();
            var userManager = serviceProvider.GetRequiredService<UserManager<AplicationUser>>();
            context.Database.EnsureCreated();
            if (!context.Users.Any())
            {
                AplicationUser user = new AplicationUser()
                {
                    Email = "test@gmail.com",
                    UserName = "test",
                    SecurityStamp = Guid.NewGuid().ToString()
                };
                userManager.CreateAsync(user, "Test@123");
            }
        }
    }//
}//
