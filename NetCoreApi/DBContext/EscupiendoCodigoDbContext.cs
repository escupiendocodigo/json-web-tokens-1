﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using NetCoreApi.Datos;

namespace NetCoreApi.DBContext
{
    public class EscupiendoCodigoDbContext : IdentityDbContext<AplicationUser>
    {
        public EscupiendoCodigoDbContext(DbContextOptions<EscupiendoCodigoDbContext> options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
    }
}
