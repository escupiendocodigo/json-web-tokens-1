﻿using EscupiendoCodigo.Persistence.Entities.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace NetFramemork.Controllers
{
    public class AuthController : Controller
    {
        [HttpGet("test")]
        public IActionResult Test()
        {

            using (SqlConnection connection = new SqlConnection("Data Source=10.10.10.12\\MOODLESQL;Initial Catalog=db_pagonomina;Integrated Security=False;User ID=sa;Password=T34mb1gb4ngDB"))
            {
                try
                {
                    connection.Open();
                    return Ok("conectado");
                }
                catch (Exception ex1)
                {
                    return Ok(ex1);

                }
            }
        }

    [HttpPost]
        [Route("login")]
        public IActionResult Login([FromBody] LoginViewModel model)
        {
            if ("jorge" == model.Username && "12345" == model.Password)
            {
                var authClaims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.Sub, model.Username),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                };
                var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("EscupiendoCódigo"));
                var token = new JwtSecurityToken(
                    issuer: "https://www.youtube.com/channel/UC4w8VIeA7H8xjrASPTuPMxA",
                    audience: "https://spittingcode.wordpress.com/",
                    expires: DateTime.Now.AddDays(1),
                    claims: authClaims,
                    signingCredentials: new Microsoft.IdentityModel.Tokens.SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                    );
                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    expiration = token.ValidTo
                });
            }
            return Unauthorized();
        }
    }//
}//