﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EscupiendoCodigo.Persistence.DB
{
    public class TestConection
    {

        public Tuple<bool,string> conection()
        {
            bool resp = false;
            string resp2 = string.Empty;
            try
            {
                using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQL"].ConnectionString))
                {
                    DataSet ds = new DataSet();
                    ds.Tables.Add(new DataTable("personas"));
                    string[] arr = { "personas" };
                    Kurama.Helpers.SqlHelper.FillDataset(ConfigurationManager.ConnectionStrings["SQL"].ConnectionString, CommandType.Text, "SELECT * FROM Cat_Clientes", ds, arr);
                    var list = ds.Tables[0];

                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                resp2 = ex.Message;
            }
            return Tuple.Create(resp,resp2);
        }
    }
}
