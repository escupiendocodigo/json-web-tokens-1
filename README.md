# Json Web Tokens

Este repositorio te ayudara a comprender comom se utiliza y crea un token utilizando la libreria de #Json Web token

## Prerrequisitos

Necesitarás lo siguiente instalado correctamente en su computadora.

* [VS2019](https://visualstudio.microsoft.com/es/) (2019 o Superior)


## Clonatelo 

* `git clone <repositorio-a-clonar-url>` Url de este repositorio
* `cd EscupiendoCodigo.JWTBearrer`

## Ejecutalo

* Una vez descargado, tienes que abrirlo con #Visual Studio y Ejecutar el proyecto
* `INICIAR` 


## Canal de Youtube
* [EscupiendCódigo](https://youtube.com/channel/UC4w8VIeA7H8xjrASPTuPMxA)

## Apoyame
* Si te gusta el contenido puedes apoyarme invitandome un pastelito
* [ApoyoAlCreador](https://www.paypal.com/paypalme/byjazr)

