﻿using EscupiendoCodigo.Persistence.DB;
using EscupiendoCodigo.Persistence.Entities.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.IdentityModel.Tokens;
using NetCoreApiDB.Roles;
using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace NetCoreApiDB.Controllers
{
    public class AuthController : Controller
    {
        
        [HttpGet("ping")]
        public IActionResult Ping()
        {
            return Ok(new { online = true });
        }

        [Authorize(Roles = Rol.Admin)]
        [HttpGet("Admin")]
        public IActionResult Admin()
        {
            return Ok(new { Admin = "loggeado" });
        }

        [Authorize(Roles = Rol.User)]
        [HttpGet("User")]
        public IActionResult UsuarioMortal()
        {
            return Ok(new { User = "loggeado" });
        }

        [HttpPost]
        [Route("login")]
        public IActionResult Login([FromBody]LoginViewModel model)
        {
            string rol = string.Empty;
            if (model.Username == "admin")
            {
                rol = Rol.Admin;
            }
            else
            {
                rol = Rol.User;
            }
            if (("user" == model.Username) ||("admin" == model.Username) && "12345" == model.Password)
            {
                // authentication successful so generate jwt token
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes("EscupiendoCódigo");
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim(ClaimTypes.Name, model.Username),
                        new Claim(ClaimTypes.Role, rol)
                    }),
                    Expires = DateTime.UtcNow.AddDays(7),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);

                return Ok(new
                {
                    token = tokenHandler.WriteToken(token)
                });
            }
            return Unauthorized();

        }

        [HttpGet("test")]
        public IActionResult Test()
        {

            using (SqlConnection connection = new SqlConnection("Data Source=10.10.10.12;Initial Catalog=db_salaprensa;Integrated Security=False;User ID=sa;Password=T34mb1gb4ngDB"))
            {
                try
                {
                    connection.Open();
                    return Ok("conectado");
                }
                catch (Exception ex1)
                {
                    return Ok(ex1);

                }
            }
        }
    }//
}//